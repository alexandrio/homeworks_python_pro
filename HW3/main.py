from flask import Flask, request
from utils import execute_sql, read_sql

app = Flask(__name__)


@app.route('/')
def index():
    return 'Web App with Python Flask!'


@app.route("/phones/create")
def phones_create():
    name = request.args['name']
    phone = request.args['phone']
    sql = f'''
    INSERT INTO Phones (contact_name, phoneValue)
    VALUES ('{name}', '{phone}');
    '''
    execute_sql(sql)
    return ""


@app.route("/phones/read")
def phones_read():
    sql = '''
    SELECT * FROM Phones;
    '''
    return read_sql(sql)


@app.route("/phones/update")
def phones_update():
    contact_id = request.args['contact_id']
    name = request.args['name']
    phone = request.args['phone']
    sql = f'''
    UPDATE Phones
    SET contact_name = '{name}', phoneValue = '{phone}'
    WHERE phoneID = {contact_id};
    '''
    execute_sql(sql)
    return ""


@app.route("/phones/delete")
def phones_delete():
    contact_id = request.args['contact_id']
    sql = f'''
    DELETE FROM Phones
    WHERE phoneID = {contact_id};
    '''
    execute_sql(sql)
    return ""


if __name__ == '__main__':
    app.run()
