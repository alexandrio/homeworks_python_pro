import sqlite3


def execute_sql(sql: str) -> None:
    con = sqlite3.connect("phones.db")
    cur = con.cursor()
    cur.execute(sql)
    con.commit()
    con.close()


def read_sql(sql: str) -> list:
    con = sqlite3.connect("phones.db")
    cur = con.cursor()
    res = cur.execute(sql)
    phones = res.fetchall()
    con.close()
    return phones
