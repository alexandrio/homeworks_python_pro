import sqlite3
con = sqlite3.connect("phones.db")
cur = con.cursor()

sql = '''
CREATE TABLE Phones (
    phoneID INTEGER PRIMARY KEY,
    contact_name varchar(255),
    phoneValue varchar(13)
);
'''
cur.execute(sql)
con.commit()
con.close()