# Реалізувати функцію parse (див. модуль main.py)
# Реалізувати функцію parse_coockie (див. модуль main.py)
#
# **написати 5 тестів (assert) до кожної функції


def parse(query: str) -> dict:
    if '?' in query:
        params = query.split('?')[1]
        if params == '':
            return {}
        else:
            params_list = params.split('&')
            if "" in params_list:
                params_list.remove("")
            result = {}
            for sub in params_list:
                key = sub.split("=")[0]
                value = sub.split("=")[1]
                result[key] = value
            return result
    else:
        return {}


if __name__ == '__main__':
    assert parse('https://example.com/path/to/page?name=ferret&color=purple') == {'name': 'ferret', 'color': 'purple'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&') == {'name': 'ferret', 'color': 'purple'}
    assert parse('http://example.com/') == {}
    assert parse('http://example.com/?') == {}
    assert parse('http://example.com/?name=John') == {'name': 'John'}

    assert parse('http://?') == {}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&size=small') == {'name': 'ferret', 'color': 'purple', 'size': 'small'}
    assert parse('https://example.com/path/to/page?name=ferret&color=purple&age=30') != {'name': 'ferret', 'color': 'purple', 'age': '31'}
    assert parse('http://example.com/?name=') == {'name': ''}
    assert type(parse('https://example.com/path/to/page?name=ferret&color=purple')) == dict


def parse_cookie(query: str) -> dict:
    if query == '':
        return {}
    else:
        cookies_list = query.split(';')
        if '' in cookies_list:
            cookies_list.remove('')
        result = {}
        for sub in cookies_list:
            key = sub.split("=", 1)[0]
            value = sub.split("=", 1)[1]
            result[key] = value
    return result


if __name__ == '__main__':
    assert parse_cookie('name=John;') == {'name': 'John'}
    assert parse_cookie('') == {}
    assert parse_cookie('name=John;age=28;') == {'name': 'John', 'age': '28'}
    assert parse_cookie('name=John=User;age=28;') == {'name': 'John=User', 'age': '28'}

    assert parse_cookie('name=John=User;age=28;number=123456') == {'name': 'John=User', 'age': '28', 'number': '123456'}
    assert parse_cookie('name=John;age=28;lastname=Watson;') == {'name': 'John', 'age': '28', 'lastname': 'Watson'}
    assert parse_cookie('=') == {'': ''}
    assert parse_cookie('name=John;age==28;') == {'name': 'John', 'age': '=28'}
    assert type(parse_cookie('name=John=User;age=28;')) != int