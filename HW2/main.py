# 1. Повертати вміст файлу з пайтон пакетами (requirements.txt)
# PATH: /requirements/ відкрити файл requirements.txt і повернути його вміст
#
# 2. Вивести 100 випадково згенерованих користувачів (пошта + ім'я) 'John aasdasda@mail.com'
# PATH: /generate-users/ ( https://pypi.org/project/Faker/ )
# + параметр який регулює кількість користувачів
#
# 3. Вивести кількість космонавтів в даний момент ((http://api.open-notify.org/astros.json) (
# https://pypi.org/project/requests/) PATH: /space/
#
# import requests
# r = requests.get('https://api.github.com/repos/psf/requests')
# r.json()["description"]
#
# Надіслати 3 файли main.py, utils.py, requirements.txt


from flask import Flask
import utils

app = Flask(__name__)


@app.route("/")
def main():
    return '<a href="/requirements">requirements.txt</a>' \
           '<p><a href="/generate-users">generate users</a>' \
           '<p><a href="/space">space</a> '


@app.route("/requirements")
def requirements():
    return utils.requirements()


@app.route("/generate-users")
def generate_users():
    return utils.generate_users()


@app.route("/space")
def space():
    return utils.space()


if __name__ == '__main__':
    app.run()
