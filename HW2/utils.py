from flask import request
import requests
from faker import Faker

fake = Faker()


def requirements():
    with open('requirements.txt') as f:
        content = f.read()
    return content


def generate_users():
    length = request.args.get('length', '100')
    users = []
    for i in range(int(length)):
        users.append(f'{fake.name()} - {fake.email()}')
    return users


def space():
    r = requests.get('http://api.open-notify.org/astros.json')
    r = r.json()
    return r
